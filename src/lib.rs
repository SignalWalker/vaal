use flint::ash;
use flint::base::{swapchain::SwapToken, VkContext};
use std::ops::Add;

use flint::winit;
use specs::prelude::*;

use std::marker::PhantomData;

use crate::render::MatStorage;
use std::time::Instant;
use winit::event::Event;
// use winit::event::WindowEvent;
use winit::event_loop::{ControlFlow, EventLoop};

pub use flint;
pub extern crate specs;
#[macro_use]
extern crate specs_derive;

pub extern crate ncollide3d;
pub extern crate nphysics3d;

use np::{
    force_generator::DefaultForceGeneratorSet,
    joint::DefaultJointConstraintSet,
    object::{DefaultBodySet, DefaultColliderSet},
    world::{DefaultGeometricalWorld, DefaultMechanicalWorld},
};
use nphysics3d as np;

use flint::nalgebra as na;

pub mod map;
pub mod net;
pub mod physics;
pub mod render;

pub trait Game: Sized {
    const NAME: &'static str = "Vaalbara Engine";
    const CLEAR_COLOR: [f32; 4] = [0.2, 0.4, 0.8, 1.0];
    const GRAVITY: [f32; 3] = [0.0, -9.81, 0.0];

    fn init(&mut self, _world: &mut World) {}

    fn respond_to_event(&mut self, _world: &mut World, _event: Event<()>) {}

    fn simulate(&mut self, world: &mut World) {
        world.insert(ControlFlow::Exit);
    }

    fn init_render(&mut self, _world: &World) {}

    fn render(&mut self, _world: &World, _cmd_buf: ash::vk::CommandBuffer) {}

    fn resized_swapchain(&mut self, _world: &World, _size: (f64, f64)) {}

    fn icon() -> Option<winit::window::Icon> {
        None
    }
}

impl Game for () {}

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct TickCount(pub f64, pub f64);

impl TickCount {
    pub fn seconds(&self) -> f64 {
        self.0 * self.1
    }

    pub fn full_ticks(&self) -> usize {
        self.1.trunc() as usize
    }
}

impl Add<&TickCount> for TickCount {
    type Output = Self;
    fn add(self, o: &TickCount) -> Self::Output {
        assert!((self.0 - o.0).abs() <= std::f64::EPSILON);
        TickCount(self.0, self.1 + o.1)
    }
}

pub struct Runner<S: Game> {
    events: Option<EventLoop<()>>,
    pub world: World,
    sim: PhantomData<S>,
}

impl<S: Game> Runner<S> {
    pub fn new(width: f64, height: f64) -> Self {
        let events = EventLoop::new();
        let window = winit::window::WindowBuilder::new()
            .with_inner_size(winit::dpi::LogicalSize::new(width, height))
            .with_resizable(true)
            .with_transparent(true)
            .with_decorations(false)
            .with_title(S::NAME.to_string())
            .with_window_icon(S::icon())
            .build(&events)
            .unwrap();
        let (vk, swapchain) = unsafe {
            VkContext::builder()
                .with_name(S::NAME)
                .with_api_version(1, 2, 141)
                .prepare_context()
                .unwrap()
                .with_device_by_type(ash::vk::PhysicalDeviceType::DISCRETE_GPU)
                .0
                .build_with_swapchain(&window)
                .unwrap()
        };

        let mut world = World::new();
        world.insert(vk);
        world.insert(swapchain);
        world.insert(MatStorage::default());
        world.insert(window);
        Self {
            events: Some(events),
            world,
            sim: PhantomData,
        }
    }

    pub fn run(mut self, mut sim: S)
    where
        S: 'static,
    {
        fn dur_to_f64(dur: std::time::Duration) -> f64 {
            (dur.as_secs() as f64 + dur.as_nanos() as f64) / 1_000_000_000.0
        }

        let dt = 0.01f64;

        self.world.insert(ControlFlow::Poll);

        let mut mech_world = DefaultMechanicalWorld::new(na::Vector3::<f32>::from(S::GRAVITY));
        mech_world.set_timestep(dt as f32);

        self.world.insert(mech_world);
        self.world.insert(DefaultGeometricalWorld::<f32>::new());
        self.world.insert(DefaultBodySet::<f32>::new());
        self.world.insert(DefaultColliderSet::<f32>::new());
        self.world.insert(DefaultJointConstraintSet::<f32>::new());
        self.world.insert(DefaultForceGeneratorSet::<f32>::new());

        sim.init(&mut self.world);

        let mut fps: f64 = 0.0;
        let mut frames: u8 = 0;
        let mut time_acc: f64 = 0.0;
        let mut frame_time = Instant::now();
        let mut render_time = Instant::now();

        let events = self.events.take().unwrap();

        #[cfg(all(target_os = "linux", feature = "fps"))]
        println!("FPS: 0");

        events.run(move |event, _win_target, ctrl_flow| {
            match event {
                Event::MainEventsCleared => {
                    time_acc += dur_to_f64(render_time.elapsed());
                    self.world.insert(TickCount(dt, time_acc / dt));
                    sim.simulate(&mut self.world);
                    render_time = Instant::now();
                    time_acc %= dt;
                    self.world
                        .read_resource::<winit::window::Window>()
                        .request_redraw();
                }
                Event::RedrawRequested { .. } => {
                    self.render(&mut sim);

                    fps = fps.mul_add(f64::from(frames), 1.0 / dur_to_f64(frame_time.elapsed()))
                        / f64::from(frames + 1);
                    frames = u8::min(12, frames + 1);

                    #[cfg(all(target_os = "linux", feature = "fps"))]
                    println!(
                        "{}{}FPS: {:.2}",
                        termion::cursor::Up(1),
                        termion::clear::CurrentLine,
                        fps
                    );
                    frame_time = Instant::now();
                }
                Event::LoopDestroyed => {
                    println!("Loop destroyed...");
                    // sim.destroy();
                }
                _ => sim.respond_to_event(&mut self.world, event),
            }
            *ctrl_flow = *self.world.read_resource::<ControlFlow>();
        });
    }

    pub fn render(&mut self, sim: &mut S) {
        sim.init_render(&self.world);

        let vk_context = self.world.read_resource::<VkContext>();
        let window = self.world.read_resource::<winit::window::Window>();

        let (present_index, resized) = unsafe {
            let mut swapchain = self.world.write_resource::<SwapToken>();
            swapchain
                .prepare_render(&vk_context.instance, window.inner_size().into())
                .unwrap()
        };

        if resized {
            sim.resized_swapchain(&self.world, window.inner_size().into());
        }

        let swapchain = self.world.read_resource::<SwapToken>();
        unsafe {
            swapchain
                .render(S::CLEAR_COLOR, present_index, |cmd_buffer| {
                    sim.render(&self.world, cmd_buffer)
                })
                .unwrap();
        }
    }
}
