use flint::nalgebra as na;
use na::Point3;
use specs::prelude::*;
use std::default::Default;
use std::ops::Add;

use std::collections::HashSet;

mod cartographer;
pub use cartographer::*;

mod kdtree;
pub use kdtree::*;

/// Mediator between Cartographers & other systems. Holds a chunk cache in a 1D Vec representing 3D space, stored as X -> Y -> Z, all increasing with vec index. When getting a chunk for use in game logic, the map first checks the cache, then, if no chunk is found, asks its cartographer for it. Also stores entities that often move between chunks or that exist outside world space.
pub struct Map {
    pub world: World,
    pub cartographer: Box<dyn Cartographer>,
    loaded_chunks: HashSet<Point3<i32>>,
}

impl Map {
    pub fn new(world: World, cartographer: Box<dyn Cartographer>) -> Self {
        // world.insert_from(
        //     (Material::from("static"), Static),
        //     vec![(Position::default(), crate::physics::Rotation::default())],
        // );
        Map {
            world,
            cartographer,
            loaded_chunks: HashSet::new(),
        }
    }

    pub fn load_chunk(&mut self, pos: &Point3<i32>) {
        if self.loaded_chunks.contains(pos) {
            return;
        }
        self.cartographer.load_chunk(pos, &mut self.world);
    }

    pub fn unload_chunk(&mut self, pos: &Point3<i32>) {
        if !self.loaded_chunks.contains(pos) {
            return;
        }
        // let _query = <(Read<GridPos>)>::query();
        unimplemented!();
    }
}

/// This assumes that the cube is facing down the Z axis
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Side {
    Top,
    Bottom,
    Left,
    Right,
    Front,
    Back,
}

/// This assumes the quad being rotated is the bottom side of a cube facing down the Z axis
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Rotation {
    Fore,
    Aft,
    Port,
    Star,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Direction {
    In,
    Out,
}

/// A terrain tile, defined as a side and a rotation. Material and shape are determined by
/// other components.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Component)]
#[storage(DenseVecStorage)]
pub struct Tile {
    pub side: Side,
    pub rot: Rotation,
    pub img_index: u8,
    pub shape: String,
    pub dir: Direction,
    pub color: [u8; 3],
}

impl Default for Tile {
    fn default() -> Self {
        Tile {
            side: Side::Bottom,
            rot: Rotation::Fore,
            img_index: 43,            // +
            shape: "tri".to_string(), // Tri
            dir: Direction::Out,
            color: [255; 3],
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Component)]
pub struct GridPos(pub Point3<u16>);

#[derive(Debug, Clone, PartialEq, Eq, Hash, Component)]
#[storage(DenseVecStorage)]
pub struct ChunkPos(pub Point3<i32>);

impl From<Point3<i32>> for ChunkPos {
    fn from(p: Point3<i32>) -> Self {
        Self(p)
    }
}

impl Add<&ChunkPos> for GridPos {
    type Output = Point3<i32>;
    fn add(self, rhs: &ChunkPos) -> Self::Output {
        use na::Vector3;
        Point3::new(
            i32::from(self.0.x),
            i32::from(self.0.y),
            i32::from(self.0.z),
        ) + Vector3::new(rhs.0.x, rhs.0.y, rhs.0.z)
    }
}
