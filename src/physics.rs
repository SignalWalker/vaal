use flint::nalgebra as na;
use na::{Matrix4, Point3, UnitQuaternion, Vector3};
use specs::Component;
use specs::DenseVecStorage;

#[derive(Debug, Clone, PartialEq, Component)]
#[storage(DenseVecStorage)]
pub struct Position(pub Point3<f32>);

impl Default for Position {
    fn default() -> Self {
        Self([0.0, 0.0, 0.0].into())
    }
}

impl<P: Into<Point3<f32>>> From<P> for Position {
    fn from(p: P) -> Self {
        Self(p.into())
    }
}

#[derive(Debug, Clone, PartialEq, Component)]
#[storage(DenseVecStorage)]
pub struct Rotation(pub UnitQuaternion<f32>);

impl Default for Rotation {
    fn default() -> Self {
        Self(UnitQuaternion::from_axis_angle(&Vector3::y_axis(), 0.0))
    }
}

impl Rotation {
    pub fn to_homogeneous(&self) -> Matrix4<f32> {
        self.0.to_homogeneous()
    }
}

impl Into<Matrix4<f32>> for &Rotation {
    fn into(self) -> Matrix4<f32> {
        self.to_homogeneous()
    }
}

#[derive(Debug, Clone, PartialEq, Component)]
#[storage(DenseVecStorage)]
pub struct Scale(pub Vector3<f32>);

impl Default for Scale {
    fn default() -> Self {
        Self(Vector3::new(1.0, 1.0, 1.0))
    }
}

#[derive(Debug, Clone, PartialEq, Component)]
#[storage(DenseVecStorage)]
pub struct Momentum(pub Vector3<f32>);

impl Default for Momentum {
    fn default() -> Self {
        Self([0.0, 0.0, 0.0].into())
    }
}

#[derive(Debug, Clone, PartialEq, Component)]
#[storage(DenseVecStorage)]
pub struct AngularMomentum(pub UnitQuaternion<f32>);
