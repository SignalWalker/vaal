mod camera;
pub use camera::*;
use flint::ash::vk;
use flint::base::SwapToken;
use flint::base::VkContext;
use flint::shader::gen_material;
use flint::shader::PushConstant;
use flint::vertex::Polyhedron;
use flint::vertex::Vertex;
use specs::Component;
use specs::DenseVecStorage;
use specs::NullStorage;
use std::collections::HashMap;
use std::iter::FromIterator;
use std::ops::Deref;
use std::ops::DerefMut;
use std::ops::Index;
use std::path::Path;

mod renderer;
pub use renderer::*;

// TODO :: static_renderer, once https://github.com/rust-lang/rust/issues/62529 is fixed
mod static_renderer;
pub use static_renderer::*;

// mod temp_static;
// pub use temp_static::*;

pub type TexID = usize;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Texture(pub TexID);

#[derive(Debug, Clone, PartialEq, Eq, Component)]
#[storage(DenseVecStorage)]
pub struct Material(pub String);

impl<S: ToString> From<S> for Material {
    fn from(s: S) -> Self {
        Self(s.to_string())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Component)]
pub struct Shape(pub String);

impl<S: ToString> From<S> for Shape {
    fn from(s: S) -> Self {
        Self(s.to_string())
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default, Component)]
#[storage(NullStorage)]
pub struct Static;

#[derive(Debug, Default)]
pub struct MatStorage(pub HashMap<String, HashMap<String, PushConstant>>);

impl Deref for MatStorage {
    type Target = HashMap<String, HashMap<String, PushConstant>>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for MatStorage {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl MatStorage {
    pub fn gen_material<S: ToString>(
        &mut self,
        vk: &VkContext,
        swapchain: &mut SwapToken,
        name: S,
        paths: Vec<(&str, &str)>,
        poly_mode: vk::PolygonMode,
    ) {
        self.0.insert(
            name.to_string(),
            gen_material(&*vk, swapchain, name.to_string(), paths, poly_mode),
        );
    }
}

#[derive(Debug, Clone)]
pub struct ShapeStorage(HashMap<String, Polyhedron<Vertex>>);

impl Index<&str> for ShapeStorage {
    type Output = Polyhedron<Vertex>;
    fn index(&self, s: &str) -> &Self::Output {
        &self.0[s]
    }
}

impl<S: ToString> FromIterator<(S, Polyhedron<Vertex>)> for ShapeStorage {
    fn from_iter<I: IntoIterator<Item = (S, Polyhedron<Vertex>)>>(iter: I) -> Self {
        ShapeStorage(
            iter.into_iter()
                .map(|(name, hedron)| (name.to_string(), hedron))
                .collect::<HashMap<_, _>>(),
        )
    }
}

impl ShapeStorage {
    pub fn add<S: ToString>(&mut self, name: S, hedron: Polyhedron<Vertex>) {
        self.0.insert(name.to_string(), hedron);
    }

    pub fn load<S: ToString, P: AsRef<Path>>(&mut self, name: S, path: P) {
        self.add(name, Polyhedron::load(path))
    }
}

#[derive(Debug)]
pub struct PushConstStorage(HashMap<String, HashMap<String, PushConstant>>);

impl Index<&str> for PushConstStorage {
    type Output = HashMap<String, PushConstant>;
    fn index(&self, s: &str) -> &Self::Output {
        &self.0[s]
    }
}
