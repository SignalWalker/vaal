use flint::nalgebra::{Matrix4, Orthographic3, Perspective3, Point3, UnitQuaternion, Vector3};
use std::ops::Mul;

#[derive(Debug, Clone, Copy)]
pub enum Projection {
    Ortho(Orthographic3<f32>),
    Persp(Perspective3<f32>),
}

impl From<Orthographic3<f32>> for Projection {
    fn from(ortho: Orthographic3<f32>) -> Self {
        Self::Ortho(ortho)
    }
}

impl From<Perspective3<f32>> for Projection {
    fn from(persp: Perspective3<f32>) -> Self {
        Self::Persp(persp)
    }
}

impl Mul<Matrix4<f32>> for &Projection {
    type Output = Matrix4<f32>;
    fn mul(self, rhs: Matrix4<f32>) -> Self::Output {
        use Projection::*;
        match self {
            Ortho(ref p) => p.as_matrix() * rhs,
            Persp(ref p) => p.as_matrix() * rhs,
        }
    }
}

#[derive(Debug)]
pub struct FreeCamera {
    pub cache: Option<Matrix4<f32>>,
    pub pos: Point3<f32>,
    pub forward: Vector3<f32>,
    pub up: Vector3<f32>,
    pub proj: Projection,
}

impl FreeCamera {
    pub fn new(pos: Point3<f32>, proj: Projection) -> Self {
        Self {
            cache: None,
            pos,
            forward: Vector3::z(),
            up: Vector3::y(),
            proj,
        }
    }

    pub fn with_persp(pos: Point3<f32>, persp: Perspective3<f32>) -> Self {
        Self::new(pos, persp.into())
    }

    pub fn new_persp(pos: Point3<f32>, aspect: f32, fov: f32, znear: f32, zfar: f32) -> Self {
        Self::with_persp(pos, Perspective3::new(aspect, fov, znear, zfar))
    }

    pub fn with_ortho(pos: Point3<f32>, ortho: Orthographic3<f32>) -> Self {
        Self::new(pos, ortho.into())
    }

    pub fn fresh_mat(&mut self) -> &Matrix4<f32> {
        if self.cache.is_none() {
            self.cache.replace(
                &self.proj * Matrix4::look_at_rh(&self.pos, &(self.pos + self.forward), &-self.up),
            );
        }
        self.cache.as_ref().unwrap()
    }

    pub fn right(&self) -> Vector3<f32> {
        self.up.cross(&self.forward) // (1, 0, 0) if up is (0, 1, 0) and forward is (0, 0, 1)
    }

    pub fn resize(&mut self, width: f32, height: f32) {
        match self.proj {
            Projection::Persp(ref mut p) => {
                p.set_aspect(width / height);
                self.cache = None
            }
            _ => unimplemented!(),
        }
    }

    pub fn set_fovy(&mut self, fov: f32) {
        match self.proj {
            Projection::Persp(ref mut p) => {
                p.set_fovy(fov);
                self.cache = None
            }
            _ => unimplemented!(),
        }
    }

    pub fn mov(&mut self, x: f32, y: f32, z: f32) {
        self.pos += self.forward * z;
        self.pos += self.up * y;
        self.pos += self.right() * x;
        self.cache = None
    }

    pub fn rot(&mut self, scaled_axis: &Vector3<f32>) {
        let q = UnitQuaternion::from_scaled_axis(*scaled_axis);
        self.forward = q * self.forward.normalize();
        self.up = q * self.up.normalize();
        self.cache = None
    }
}
