use crate::render::camera::Projection;
use flint::nalgebra as na;
use na::Point3;
use std::f32::consts::PI;

const TWO_PI: f32 = 2.0 * PI;
const HALF_PI: f32 = PI / 2.0;

pub struct OrbitCamera {
    pub target: Point3<f32>,
    pub rad: f32,
    pub pol: f32,
    pub azi: f32,
    pub roll: f32,
    pub projection: Projection,
    cache: Option<Matrix4<f32>>,
}

impl OrbitCamera {
    pub fn new(rad: f32, pol: f32, azi: f32, target: f32, proj: Projection) -> Self {
        OrbitCamera {
            cache: None,
            rad,
            pol,
            azi,
            proj,
        }
    }

    pub fn with_persp(rad: f32, pol: f32, azi: f32, persp: Perspective3<f32>) -> Self {
        Self::new(rad, pol, azi, persp.into())
    }

    pub fn new_persp(
        rad: f32,
        pol: f32,
        azi: f32,
        aspect: f32,
        fov: f32,
        znear: f32,
        zfar: f32,
    ) -> Self {
        Self::with_persp(rad, pol, azi, Perspective3::new(aspect, fov, znear, zfar))
    }

    pub fn with_ortho(rad: f32, pol: f32, azi: f32, ortho: Orthographic3<f32>) -> Self {
        Self::new(rad, pol, azi, ortho.into())
    }

    pub fn pos(&self) -> Point3<f32> {
        let rad_pol_sin = self.rad * self.pol.sin();
        Point3::from([
            rad_pol_sin * self.azi.cos(),
            rad_pol_sin * self.azi.sin(),
            self.rad * self.pol.cos(),
        ])
    }

    pub fn forward(&self) -> Vector3<f32> {
        self.target - self.pos()
    }

    pub fn up(&self) -> Vector3<f32> {
        unimplemented!()
    }

    pub fn right(&self) -> Vector3<f32> {
        self.up().cross(&self.forward())
    }

    pub fn zoom(&mut self, amt: f32) {
        self.rad += amt;
        self.cache = None;
    }

    pub fn spin(&mut self, azi: f32, pol: f32) {
        let sin = self.roll.sin();
        let cos = self.roll.cos();
        self.azi = (self.azi + azi * cos - pol * sin) % TWO_PI;
        self.pol = (self.pol + azi * sin + pol * cos).clamp(0.0, PI);
    }

    pub fn spin_deg(&mut self, azi: f32, pol: f32) {
        self.spin(azi.to_radians(), pol.to_radians())
    }

    pub fn rot(&mut self, amt: f32) {
        self.roll = (self.roll + amt) % TWO_PI;
    }

    pub fn rot_deg(&mut self, amt: f32) {
        self.rot(amt.to_radians())
    }
}
