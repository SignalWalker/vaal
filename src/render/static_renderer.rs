use crate::map::ChunkPos;
use crate::map::{Direction, GridPos, Side, Tile};
use crate::physics::Position;
use crate::physics::Rotation;
use crate::physics::Scale;
use crate::render::ShapeStorage;
use flint::base::VkContext;
// use crate::render::renderer::Renderer;
use crate::render::Material;
use crate::render::Shape;
use crate::render::Static;
use specs::join::JoinParIter;
use std::f32::consts::PI;
use std::iter::FromIterator;

use std::sync::RwLock;

use ash::{vk, Device};
use flint::ash;
use flint::base::swapchain::SwapToken;

use flint::vertex::{model::Model, Polyhedron, Vertex};

use std::collections::HashMap;

use std::sync::Mutex;

use specs::prelude::*;

use crate::render::Renderer;
use flint::nalgebra as na;
use na::{Matrix3, Matrix4, Point3, Similarity2, Translation3, UnitQuaternion, Vector2, Vector3};

#[derive(Debug)]
pub struct StaticModel {
    pub model: Model,
    pub material: String,
}

impl StaticModel {
    pub fn new(model: Model, material: String) -> Self {
        Self { model, material }
    }

    pub fn generate<C: ParJoin, Acc>(
        dev: Device,
        mem_prop: &vk::PhysicalDeviceMemoryProperties,
        iter: C,
        acc: Acc,
    ) -> HashMap<String, Result<Self, ()>>
    where
        JoinParIter<C>: ParallelIterator,
        Acc: Copy
            + Send
            + Sync
            + Fn(<JoinParIter<C> as ParallelIterator>::Item) -> (Polyhedron<Vertex>, String),
    {
        let hedrons: RwLock<HashMap<String, Mutex<Option<Polyhedron<Vertex>>>>> =
            RwLock::new(HashMap::new());

        iter.par_join().for_each(|v| {
            let (res, mat) = acc(v);
            if hedrons.read().unwrap().get(&mat).is_none() {
                hedrons
                    .write()
                    .unwrap()
                    .insert(mat.clone(), Mutex::new(None));
            }
            {
                let hedrons = hedrons.read().unwrap();
                let mut hedron = hedrons.get(&mat).as_ref().unwrap().lock().unwrap();
                match &mut *hedron {
                    Some(h) => h.merge(res),
                    None => {
                        (*hedron).replace(res);
                    }
                }
            }
        });
        HashMap::from_iter(
            hedrons
                .into_inner()
                .unwrap()
                .into_iter()
                .map(|(mat, hedron)| match hedron.into_inner().unwrap() {
                    Some(mut hedron) => {
                        hedron.calc_normals();
                        println!(
                            "hedron :: verts: {}, faces: {}",
                            hedron.points.len(),
                            hedron.faces.len()
                        );
                        (
                            mat.clone(),
                            Ok(StaticModel::new(
                                Model::new(dev.clone(), mem_prop, hedron),
                                mat,
                            )),
                        )
                    }
                    None => (mat, Err(())),
                }),
        )
    }

    pub fn render(&self, cmd_buf: ash::vk::CommandBuffer, swapchain: &SwapToken) {
        swapchain.pipes[&self.material].bind(cmd_buf);
        self.model.bind(cmd_buf);
        self.model.draw(cmd_buf);
    }
}

pub struct StaticRenderer {
    statics: HashMap<String, StaticModel>,
}

impl Default for StaticRenderer {
    fn default() -> Self {
        Self::new()
    }
}

impl StaticRenderer {
    pub fn new() -> Self {
        Self {
            statics: HashMap::new(),
        }
    }

    pub fn gen_static_meshes<C: ParJoin, Acc>(
        &mut self,
        dev: Device,
        mem_prop: &vk::PhysicalDeviceMemoryProperties,
        iter: C,
        acc: Acc,
    ) where
        JoinParIter<C>: ParallelIterator,
        Acc: Copy
            + Send
            + Sync
            + Fn(<JoinParIter<C> as ParallelIterator>::Item) -> (Polyhedron<Vertex>, String),
    {
        self.statics = HashMap::from_iter(
            StaticModel::generate::<C, Acc>(dev, mem_prop, iter, acc)
                .into_iter()
                .filter_map(|(mat, hedron)| match hedron {
                    Err(_) => None,
                    Ok(h) => Some((mat, h)),
                }),
        );
    }

    pub fn gen_generic_meshes(&mut self, world: &World) {
        let base_models = world.read_resource::<ShapeStorage>();
        let vk = world.read_resource::<VkContext>();
        self.gen_static_meshes(
            vk.device.clone(),
            &(*vk).device_memory_properties,
            (
                &world.read_storage::<Static>(),
                &world.read_storage::<Material>(),
                &world.read_storage::<Shape>(),
                &world.read_storage::<Position>(),
                &world.read_storage::<Rotation>(),
                &world.read_storage::<Scale>(),
            ),
            |(_stat, mat, shape, pos, rot, scale)| {
                let mut model = base_models[&shape.0].clone();
                model.transform(&{
                    let translate = Translation3::new(pos.0.x, pos.0.y, pos.0.z);
                    translate.to_homogeneous()
                        * rot.clone().to_homogeneous()
                        * Matrix4::new_nonuniform_scaling(&scale.0)
                });
                (model, mat.0.clone())
            },
        );
    }

    pub fn gen_terrain_meshes(&mut self, world: &World) {
        fn get_tex(i: u8) -> Matrix3<f32> {
            const TILE_SIZE: f32 = 0.0625;
            let tx = f32::from(i % 16);
            let ty = f32::from(i / 16);
            Similarity2::new(Vector2::new(TILE_SIZE * tx, TILE_SIZE * ty), 0.0, TILE_SIZE)
                .to_homogeneous()
        }
        let base_models = world.read_resource::<ShapeStorage>();
        let vk = world.read_resource::<VkContext>();
        self.gen_static_meshes(
            vk.device.clone(),
            &(*vk).device_memory_properties,
            (
                &world.read_storage::<Static>(),
                &world.read_storage::<Material>(),
                &world.read_storage::<GridPos>(),
                &world.read_storage::<Tile>(),
                &world.read_storage::<ChunkPos>(),
            ),
            |(_stat, mat, pos, tile, chunk)| {
                let mut model = (*base_models)[&tile.shape].clone();
                model.transform(&{
                    // let s_rot = match tile.side {
                    //     Side::Top => 0,
                    //     Side::Bottom => 4,
                    //     Side::Left => 8,
                    //     Side::Right => 12,
                    //     Side::Front => 16,
                    //     Side::Back => 20,
                    // };
                    // let rot = match tile.rot {
                    //     Rotation::Fore => rots[s_rot],
                    //     Rotation::Aft => rots[s_rot + 1],
                    //     Rotation::Port => rots[s_rot + 2],
                    //     Rotation::Star => rots[s_rot + 3],
                    // };
                    let trans_to_side = Translation3::new(0.0, -0.5, 0.0);
                    let rot = match tile.side {
                        Side::Top => {
                            Matrix4::new_rotation_wrt_point(Vector3::z() * PI, Point3::origin())
                        }
                        Side::Bottom => Matrix4::identity(),
                        Side::Left => Matrix4::new_rotation_wrt_point(
                            Vector3::z() * (PI / 2.0),
                            Point3::origin(),
                        ),
                        Side::Right => Matrix4::new_rotation_wrt_point(
                            Vector3::z() * (-PI / 2.0),
                            Point3::origin(),
                        ),
                        Side::Front => Matrix4::new_rotation_wrt_point(
                            Vector3::x() * (-PI / 2.0),
                            Point3::origin(),
                        ),
                        Side::Back => Matrix4::new_rotation_wrt_point(
                            Vector3::x() * (PI / 2.0),
                            Point3::origin(),
                        ),
                    };
                    let flip = match tile.dir {
                        Direction::In => Matrix4::identity(),
                        Direction::Out => {
                            UnitQuaternion::from_scaled_axis(Vector3::z() * PI).to_homogeneous()
                        }
                    };
                    let pos = *pos + chunk;
                    let translate = Translation3::new(pos.x as f32, pos.y as f32, pos.z as f32)
                        .to_homogeneous();
                    translate * rot * trans_to_side.to_homogeneous() * flip
                });
                model.transform_uv(&get_tex(tile.img_index));
                let color = [
                    tile.color[0] as f32 / 255.0,
                    tile.color[1] as f32 / 255.0,
                    tile.color[2] as f32 / 255.0,
                ];
                model.par_for_each(|vert| vert.color_mut().copy_from_slice(&color));
                (model, mat.0.clone())
            },
        )
    }
}

impl Renderer for StaticRenderer {
    fn render(&self, world: &World, cmd_buf: vk::CommandBuffer) {
        let swapchain = world.read_resource::<SwapToken>();
        for (_mat, stat) in self.statics.iter() {
            stat.render(cmd_buf, &swapchain);
        }
    }
}

// impl<'w> System<'w> for StaticRenderer {
//     type SystemData = (
//         Read<'w, ash::vk::CommandBuffer>,
//         Read<'w, Option<SwapToken>>,
//     );
//     fn run(&mut self, (cmd_buf, swapchain): Self::SystemData) {
//         self.render(*cmd_buf, swapchain.as_ref().unwrap());
//     }
// }
