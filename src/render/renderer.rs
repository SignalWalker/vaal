use flint::ash::vk;

use specs::World;

pub trait Renderer {
    fn render(&self, _world: &World, _cmd_buf: vk::CommandBuffer) {}

    fn chain<B: Renderer>(self, other: B) -> Chain<Self, B>
    where
        Self: Sized,
    {
        Chain { a: self, b: other }
    }
}

pub struct Chain<A: Renderer, B: Renderer> {
    pub a: A,
    pub b: B,
}

impl<A: Renderer, B: Renderer> Renderer for Chain<A, B> {
    fn render(&self, world: &World, cmd_buf: vk::CommandBuffer) {
        self.a.render(world, cmd_buf);
        self.b.render(world, cmd_buf);
    }
}
