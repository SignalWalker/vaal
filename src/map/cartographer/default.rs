#[derive(Debug, Default)]
pub struct MinCartographer;
impl Cartographer for MinCartographer {
    fn load_chunk(&mut self, pos: &Point3<i32>, world: &mut World) {
        world.insert_from(
            (Material::from("static"), ChunkPos(*pos), Static),
            vec![(
                Tile {
                    img_index: 1,
                    shape: 1,
                    ..Default::default()
                },
                GridPos([0, 0, 0].into()),
            )],
        );
    }
}

pub struct PlainCartographer {
    material: Material,
}

impl Default for PlainCartographer {
    fn default() -> Self {
        Self {
            material: Material::from("static"),
        }
    }
}

impl Cartographer for PlainCartographer {
    fn load_chunk(&mut self, pos: &Point3<i32>, world: &mut World) {
        if pos.y != 0 {
            return;
        }
        let mut rng = rand::thread_rng();
        let shared = (self.material.clone(), ChunkPos(*pos), Static);
        let mut tiles = Vec::new();
        for x in 0..12 {
            for z in 0..12 {
                tiles.push((
                    Tile {
                        img_index: rng.gen::<u8>(),
                        shape: 1,
                        ..Default::default()
                    },
                    GridPos([x, 6, z].into()),
                ));
            }
        }
        world.insert_from(shared, tiles);
    }
}

#[derive(Debug, Default)]
pub struct ChessCartographer;

impl Cartographer for ChessCartographer {
    fn load_chunk(&mut self, pos: &Point3<i32>, world: &mut World) {
        if pos.y != 0 {
            return;
        }
        let white_shared = (Material::from("static"), ChunkPos(*pos), Static);
        let red_shared = (Material::from("wire"), ChunkPos(*pos), Static);
        let mut white = Vec::new();
        let mut red = Vec::new();
        for x in 0..8 {
            for z in 0..8 {
                let tile = (
                    Tile {
                        img_index: 43,
                        shape: 1,
                        ..Default::default()
                    },
                    GridPos([x, 0, z].into()),
                );
                if (x + z) % 2 == 0 {
                    white.push(tile);
                } else {
                    red.push(tile);
                }
                // Place Pawns
                if z < 2 || z > 5 {
                    let pawn = (
                        Tile {
                            img_index: 219,
                            shape: 4,
                            ..Default::default()
                        },
                        GridPos([x, 1, z].into()),
                    );
                    if z < 2 {
                        white.push(pawn);
                    } else {
                        red.push(pawn);
                    }
                }
            }
        }
        world.insert_from(white_shared, white);
        world.insert_from(red_shared, red);
    }
}

#[derive(Debug, Default)]
pub struct TestCartographer;

impl Cartographer for TestCartographer {
    fn load_chunk(&mut self, pos: &Point3<i32>, world: &mut World) {
        let mut tiles = Vec::new();
        let mut rng = rand::thread_rng();
        for x in 0..12 {
            for y in 0..12 {
                for z in 0..12 {
                    let tile = (
                        Tile {
                            side: match rng.gen::<u8>() % 6 {
                                0 => Side::Top,
                                1 => Side::Bottom,
                                2 => Side::Left,
                                3 => Side::Right,
                                4 => Side::Front,
                                5 => Side::Back,
                                _ => Side::Bottom,
                            },
                            rot: match rng.gen::<u8>() % 4 {
                                0 => Rotation::Fore,
                                1 => Rotation::Aft,
                                2 => Rotation::Port,
                                3 => Rotation::Star,
                                _ => Rotation::Fore,
                            },
                            img_index: rng.gen::<u8>(),
                            shape: rng.gen::<u8>() % 4,
                            dir: if rng.gen::<bool>() {
                                Direction::In
                            } else {
                                Direction::Out
                            },
                            color: [rng.gen::<u8>(), rng.gen::<u8>(), rng.gen::<u8>()],
                        },
                        GridPos([x, y, z].into()),
                    );
                    tiles.push(tile);
                }
            }
        }
        let shared = (ChunkPos(*pos), Material::from("static"), Static);
        world.insert_from(shared, tiles);
    }
}