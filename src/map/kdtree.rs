use birch::Tree;
use either::*;
use flint::nalgebra as na;
use std::collections::VecDeque;
// use std::iter::FromIterator;

type Point3f = na::Point3<f32>;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Axis {
    X,
    Y,
    Z,
}

impl Axis {
    fn next(&self) -> Self {
        use Axis::*;
        match self {
            X => Y,
            Y => Z,
            Z => X,
        }
    }
}

impl Into<usize> for Axis {
    fn into(self) -> usize {
        use Axis::*;
        match self {
            X => 0,
            Y => 1,
            Z => 2,
        }
    }
}

pub struct KdTree<V>(pub Tree<Either<f32, (Point3f, V)>>);

impl<V> KdTree<V> {
    pub fn construct(points: Vec<(Point3f, V)>) -> Self {
        use Axis::*;
        use Either::*;
        fn split<V>(
            axis: Axis,
            mut pts: Vec<(Point3f, V)>,
        ) -> (Vec<(Point3f, V)>, Vec<(Point3f, V)>) {
            let i: usize = axis.into();
            pts.sort_by(|a, b| a.0[i].partial_cmp(&b.0[i]).unwrap());
            let right = pts.split_off(pts.len() / 2);
            (pts, right)
        }

        let mut split_queue = VecDeque::from({
            let (left, right) = split(X, points);
            vec![(0, X, left), (0, X, right)]
        });
        let mut tree = Tree::new(Left(0.0));
        while !split_queue.is_empty() {
            let (parent, p_axis, mut points) = split_queue.pop_front().unwrap();
            let axis = p_axis.next();
            match points.len() {
                0 => continue,
                1 => {
                    let p = points.remove(0);
                    tree.0.vert_mut(parent).val = Right(p);
                }
                _ => {
                    let (left, right) = split(axis, points);
                    tree.0.vert_mut(parent).val = Left(right[0].0[p_axis.into()]);
                    let lp = tree.add_child(parent, Left(0.0));
                    let rp = tree.add_child(parent, Left(0.0));
                    split_queue.push_back((lp, axis, left));
                    split_queue.push_back((rp, axis, right));
                }
            }
        }
        KdTree(tree)
    }

    pub fn nearest(&self, pos: &Point3f) -> &(Point3f, V) {
        let (_, n) = self.r_nearest(None, Axis::X, 0, pos);
        (self.0).0.vert(n).val.as_ref().right().unwrap()
    }

    pub fn r_nearest(
        &self,
        best: Option<(f32, usize)>,
        axis: Axis,
        n: usize,
        pos: &Point3f,
    ) -> (f32, usize) {
        use std::cmp::Ordering;
        use Either::*;
        let node = (self.0).0.vert(n);
        match node.val {
            Right((n_pos, ref _v)) => {
                let dist = na::distance(pos, &n_pos);
                if best.is_none() || best.unwrap().0 > dist {
                    (dist, n)
                } else {
                    best.unwrap()
                }
            }
            Left(r_val) => {
                let p_val = pos[axis.into()];
                let next = match p_val.partial_cmp(&r_val).unwrap() {
                    Ordering::Less => 0,
                    Ordering::Greater | Ordering::Equal => 1,
                };
                self.r_nearest(best, axis.next(), self.0.children(n)[next], pos)
            }
        }
    }
}
