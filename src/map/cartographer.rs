use super::na::Point3;
use specs::World;

// mod default;
// pub use default::*;

/// Describes systems that load data into worlds.
///
/// "Somewhere" can be a place where the data already exists (Your own hard drive or a remote server) or an algorithm.
pub trait Cartographer {
    fn load_chunk(&mut self, _pos: &Point3<i32>, _world: &mut World) {}
}

impl Cartographer for () {}
