use std::net::{UdpSocket};
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};

/// # Net
/// ## Formats
/// ### System
/// #### Connect
/// #### ConnectAccept
/// #### Disconnect

pub trait Serialize<T> {
    fn serialize(&self) -> T;
}

pub trait Deserialize<T> {
    fn deserialize(data: T) -> Self;
}

pub trait NetMsg<T> {
    fn recv(msg: T);
}

pub struct Server {
    pub udp: UdpSocket,
    pub send_queue: Receiver<Vec<u8>>,
}

impl Server {
    pub fn new(addr: &str) -> Result<(Server, Sender<Vec<u8>>), std::io::Error> {
        let (send, recv) = mpsc::channel();
        Ok((Server {
            udp: UdpSocket::bind(addr)?,
            send_queue: recv
        }, send))
    }
}
